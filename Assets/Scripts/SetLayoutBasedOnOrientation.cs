﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetLayoutBasedOnOrientation : MonoBehaviour
{
    enum PanelsOrientation { None, Horizontal, Vertical };

    PanelsOrientation lastValue;
    PanelsOrientation panelsOrientation;

    private void OnRectTransformDimensionsChange()
    {
        float currentAspectRatio = Camera.main.aspect;

        panelsOrientation = (currentAspectRatio >= 1) ? PanelsOrientation.Horizontal : PanelsOrientation.Vertical;

        if (lastValue != panelsOrientation)
        {
            RearrangePanels();
            lastValue = panelsOrientation;
        }
    }

    private void RearrangePanels()
    {
        Debug.Log("RearrangePanels " + panelsOrientation);

        HorizontalOrVerticalLayoutGroup newGroup = null;

        switch (panelsOrientation)
        {
            case PanelsOrientation.Horizontal:
                DestroyImmediate(GetComponent<VerticalLayoutGroup>());
                newGroup = gameObject.AddComponent<HorizontalLayoutGroup>();
                break;
            case PanelsOrientation.Vertical:
                DestroyImmediate(GetComponent<HorizontalLayoutGroup>());
                newGroup = gameObject.AddComponent<VerticalLayoutGroup>();
                break;
        }

        if (newGroup != null)
        {
            newGroup.spacing = 10;
            newGroup.padding = new RectOffset(10, 10, 10, 10);
        }

    }
}
